const {Op} = require('sequelize')
const Purchase = require('../models/Purchase');
const Product = require('../models/Product');
const Client = require('../models/Client');


const create = async(req,res) => {
    try {
        const purchase = await Purchase.create(req.body);
        console.log(req.body);

        return res.status(200).json({msg: "Compra cadastrada.", purchase: purchase});

    } catch(err) {
        res.status(500).json({error: err});

    }

};

const index = async(req, res) =>{
    try {
        const purchase = await Purchase.findAll();

        return res.status(200).json({purchase});

    } catch(err) {
        console.log(err.message)
        return res.status(500).json({err});

    }

};

const show = async(req,res) => {
    const {id} = req.params;

    try {
        const purchase = await Purchase.findByPk(id);

        return res.status(200).json({purchase});

    } catch(err) {
        return res.status(500).json({err});

    }
    
};

const update = async(req,res) => {
    const {id} = req.params;

    try {
        const [updated] = await Purchase.update(req.body, {where: {id: id}});
        
        if(updated) {
            const purchase = await Purchase.findByPk(id);

            return res.status(200).send(purchase);
        }

        throw new Error();

    } catch(err) {
        return res.status(500).json("Compra não encontrada");

    }

};

const destroy = async(req,res) => {
    const {id} = req.params;

    try {
        const deleted = await Purchase.destroy({where: {id: id}});

        if(deleted) {
            return res.status(200).json("Compra deletada.");

        }

        throw new Error();

    } catch(err) {
        return res.status(500).json("Compra não encontrada.");

    }

};

const performPurchase = async(req,res) => {
    const {clientId} = req.params;

    try {
        const product = await Product.findByPk(req.body.productId);
        const client = await Client.findByPk(clientId);
        await product.setUser(client);

        return res.status(200).json({msg: "Compra concluída."});

    } catch(err) {
        return res.status(500).json({err});

    }

};

const cancelPurchase = async(req,res) => {
    const {clientId} = req.params;

    try {
        const client = await Client.findByPk(clientId);
        await client.setUser(null);

        return res.status(200).json({msg: "Compra cancelada."});

    } catch(err) {
        return res.status(500).json({err});

    }

};

module.exports = {
    create,
    index,
    show,
    update,
    destroy,
    performPurchase,
    cancelPurchase
    
}