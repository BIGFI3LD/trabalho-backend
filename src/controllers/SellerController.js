const {Op} = require('sequelize')
const Seller = require('../models/Seller');


const create = async(req,res) => {
    try {
        const seller = await Seller.create(req.body);
        console.log(req.body);

        return res.status(200).json({msg: "Vendedor cadastrado.", seller: seller});

    } catch(err) {
        res.status(500).json({error: err});

    }

};

const index = async(req, res) =>{
    try{
        const seller = await Seller.findAll();

        return res.status(200).json({seller});

    } catch(err) {
        console.log(err.message)

        return res.status(500).json({err});

    }

};

const show = async(req,res) => {
    const {id} = req.params;

    try {
        const seller = await Seller.findByPk(id);

        return res.status(200).json({seller});

    } catch(err) {
        return res.status(500).json({err});

    }

};

const update = async(req,res) => {
    const {id} = req.params;

    try {
        const [updated] = await Seller.update(req.body, {where: {id: id}});
        
        if(updated) {
            const seller = await Seller.findByPk(id);

            return res.status(200).send(seller);

        }

        throw new Error();

    } catch(err) {
        return res.status(500).json("Vendedor não encontrado.");

    }

};

const destroy = async(req,res) => {
    const {id} = req.params;
    
    try {
        const deleted = await Seller.destroy({where: {id: id}});
        
        if(deleted) {
            return res.status(200).json("Vendedor deletado.");

        }

        throw new Error();

    } catch(err) {
        return res.status(500).json("Vendedor não encontrado.");

    }

};


module.exports = {
    create,
    index,
    show,
    update,
    destroy,
    
}