const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");


const Product = sequelize.define('Product', {
    name:{
        type: DataTypes.STRING,
        allowNull:false

    },

    description:{
        type: DataTypes.STRING,
        allowNull:false

    },

    price: {
        type: DataTypes.INTEGER,
        allowNull: false

    },

    evaluation: {
        type: DataTypes.STRING,
        allowNull: false

    },

    quantity: {
        type: DataTypes.INTEGER,
        allowNull: false

    },

    photograph:{
        type: DataTypes.TEXT,
        allowNull:false

    }

});


Product.associate = function(models) {
    Product.belongsTo(models.Purchase);
    Product.belongsTo(models.Seller);

};


module.exports = Product;