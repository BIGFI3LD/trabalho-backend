const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");


const Purchase = sequelize.define('Purchase', {
    amount: {
        type: DataTypes.DOUBLE,
        allowNull: false

    },

    discount: {
        type: DataTypes.DOUBLE,
        allowNull: false

    },

    date: {
        type: DataTypes.DATEONLY,
        allowNull: false

    },

    paymentMethod: {
        type: DataTypes.STRING,
        allowNull: false

    },

    clientAdress: {
        type: DataTypes.STRING,
        allowNull: false

    },
    
});


Purchase.associate = function(models) {
    Purchase.belongsTo(models.Client);
    Purchase.hasMany(models.Product);
    
};


module.exports = Purchase;