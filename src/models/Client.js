const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");


const Client = sequelize.define('Client', {
    name: {
        type: DataTypes.STRING,
        allowNull: false

    },

    birthday: {
        type: DataTypes.DATEONLY,
        allowNull: false

    },

    phoneNumber: {
        type: DataTypes.STRING,
        allowNull: false

    },

    cpf: {
        type: DataTypes.STRING,
        allowNull: false

    },

    email: {
        type: DataTypes.STRING,
        allowNull: false

    },

    password: {
        type: DataTypes.STRING,
        allowNull: false

    },
    
});


Client.associate = function(models) {
    Client.hasMany(models.Purchase);
    
};


module.exports = Client;