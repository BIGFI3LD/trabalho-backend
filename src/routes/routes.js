const { Router } = require("express");
const router = Router();
const ClientController = require("../controllers/ClientController");
const PurchaseController = require("../controllers/PurchaseController");
const ProductController = require("../controllers/ProductController");
const SellerController = require("../controllers/SellerController");


//      Client routes    // 
router.post("/Client", ClientController.create);
router.get("/Client/:id", ClientController.show); 
router.get("/Client", ClientController.index); 
router.put("/Client/:id", ClientController.update);
router.delete("/Client/:id", ClientController.destroy);


//      Product routes    // 
router.post("/Product", ProductController.create);
router.get("/Product/:id", ProductController.show); 
router.get("/Product", ProductController.index); 
router.put("/Product/:id", ProductController.update);
router.delete("/Product/:id", ProductController.destroy);


//      Purchase routes    // 
router.post("/Purchase", PurchaseController.create);
router.get("/Purchase/:id", PurchaseController.show); 
router.get("/Purchase", PurchaseController.index); 
router.put("/Purchase/:id", PurchaseController.update);
router.delete("/Purchase/:id", PurchaseController.destroy);
router.put("/Purchase/performPurchase/:ClientId", PurchaseController.performPurchase);
router.put("/Purchase/cancelPurchase/:id",PurchaseController.cancelPurchase);


//      Seller routes    //
router.post("/Seller", SellerController.create);
router.get("/Seller/:id", SellerController.show); 
router.get("/Seller", SellerController.index); 
router.put("/Seller/:id", SellerController.update);
router.delete("/Seller/:id", SellerController.destroy);


module.exports = router;